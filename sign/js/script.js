const signInBtn = document.getElementById("signIn");
const signUpBtn = document.getElementById("signUp");
const fistForm = document.getElementById("form1");
const secondForm = document.getElementById("form2");
const container = document.querySelector(".container");

signInBtn.addEventListener("click", () => {
	container.classList.remove("right-panel-active");
});

signUpBtn.addEventListener("click", () => {
	container.classList.add("right-panel-active");
});

fistForm.addEventListener("submit", (e) => e.preventDefault());
secondForm.addEventListener("submit", (e) => e.preventDefault());

//var img = document.querySelector('img'); //获取img
//var date = new Date();  //创建一个对象
//var h = date.getHours();
//if(h<12){
//	img.src = "sign/img/morning.png";  //根据时间修改图片
//}else if(h<18){
//	img.src = "sign/img/noon.png";
//}else{
//	img.src = "sign/img/night.png";
//}

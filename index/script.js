new Vue({
    el: "#app",
    data() {
        return {
            audio: null,
            circleLeft: null,
            barWidth: null,
            duration: null,
            currentTime: null,
            isTimerPlaying: false,
            tracks: [{
                name: "Seikka/钊浩/JIA",
                artist: "2021乘梦远航",
                cover: "/img/cmyh.png",
                source: "/music/2021cmyh.mp3",
                url: "/download/index.html",
                favorited: true
            }, {
                name: "Seikka/钊浩/JIA",
                artist: "乘梦远航",
                cover: "/img/cmyh.png",
                source: "/music/cmyh.mp3",
                url: "/download/index.html",
                favorited: true
            }, {
                name: "Seikka/钊浩",
                artist: "我们的",
                cover: "/img/ours.png",
                source: "/music/ours.mp3",
                url: "/download/index.html",
                favorited: true
            }, {
                name: "Seikka",
                artist: "僕と夢",
                cover: "/img/mydream.png",
                source: "/music/mydream.mp3",
                url: "/download/index.html",
                favorited: true
            }, {
                name: "Seikka/JIA.",
                artist: "围巾",
                cover: "/img/weijin.png",
                source: "/music/weijin.mp3",
                url: "/download/index.html",
                favorited: true
            }, {
                name: "Seikka",
                artist: "谁与云归",
                cover: "/img/back.png",
                source: "/music/back.mp3",
                url: "/download/index.html",
                favorited: true
            }, {
                name: "Seikka",
                artist: "疲惫的雨",
                cover: "/img/tiredrain.png",
                source: "/music/TiredRain.mp3",
                url: "/download/index.html",
                favorited: true
            }, {
                name: "Seikka",
                artist: "雨が降り声",
                cover: "/img/rain.png",
                source: "/music/rain.mp3",
                url: "/download/index.html",
                favorited: true
            }],
            currentTrack: null,
            currentTrackIndex: 0,
            transitionName: null
        };
    },
    methods: {
        play() {
            if (this.audio.paused) {
                this.audio.play();
                this.isTimerPlaying = true;
            } else {
                this.audio.pause();
                this.isTimerPlaying = false;
            }
        },
        generateTime() {
            let width = (100 / this.audio.duration) * this.audio.currentTime;
            this.barWidth = width + "%";
            this.circleLeft = width + "%";
            let durmin = Math.floor(this.audio.duration / 60);
            let dursec = Math.floor(this.audio.duration - durmin * 60);
            let curmin = Math.floor(this.audio.currentTime / 60);
            let cursec = Math.floor(this.audio.currentTime - curmin * 60);
            if (durmin < 10) {
                durmin = "0" + durmin;
            }
            if (dursec < 10) {
                dursec = "0" + dursec;
            }
            if (curmin < 10) {
                curmin = "0" + curmin;
            }
            if (cursec < 10) {
                cursec = "0" + cursec;
            }
            this.duration = durmin + ":" + dursec;
            this.currentTime = curmin + ":" + cursec;
        },
        updateBar(x) {
            let progress = this.$refs.progress;
            let maxduration = this.audio.duration;
            let position = x - progress.offsetLeft;
            let percentage = (100 * position) / progress.offsetWidth;
            if (percentage > 100) {
                percentage = 100;
            }
            if (percentage < 0) {
                percentage = 0;
            }
            this.barWidth = percentage + "%";
            this.circleLeft = percentage + "%";
            this.audio.currentTime = (maxduration * percentage) / 100;
            this.audio.play();
        },
        clickProgress(e) {
            this.isTimerPlaying = true;
            this.audio.pause();
            this.updateBar(e.pageX);
        },
        prevTrack() {
            this.transitionName = "scale-in";
            this.isShowCover = false;
            if (this.currentTrackIndex > 0) {
                this.currentTrackIndex--;
            } else {
                this.currentTrackIndex = this.tracks.length - 1;
            }
            this.currentTrack = this.tracks[this.currentTrackIndex];
            this.resetPlayer();
        },
        nextTrack() {
            this.transitionName = "scale-out";
            this.isShowCover = false;
            if (this.currentTrackIndex < this.tracks.length - 1) {
                this.currentTrackIndex++;
            } else {
                this.currentTrackIndex = 0;
            }
            this.currentTrack = this.tracks[this.currentTrackIndex];
            this.resetPlayer();
        },
        resetPlayer() {
            this.barWidth = 0;
            this.circleLeft = 0;
            this.audio.currentTime = 0;
            this.audio.src = this.currentTrack.source;
            setTimeout(()=>{
                if (this.isTimerPlaying) {
                    this.audio.play();
                } else {
                    this.audio.pause();
                }
            }
            , 300);
        },
        favorite() {
            this.tracks[this.currentTrackIndex].favorited = !this.tracks[this.currentTrackIndex].favorited;
        }
    },
    created() {
        let vm = this;
        this.currentTrack = this.tracks[0];
        this.audio = new Audio();
        this.audio.src = this.currentTrack.source;
        this.audio.ontimeupdate = function() {
            vm.generateTime();
        }
        ;
        this.audio.onloadedmetadata = function() {
            vm.generateTime();
        }
        ;
        this.audio.onended = function() {
            vm.nextTrack();
            this.isTimerPlaying = true;
        }
        ;
        for (let index = 0; index < this.tracks.length; index++) {
            const element = this.tracks[index];
            let link = document.createElement('link');
            link.rel = "prefetch";
            link.href = element.cover;
            link.as = "image"
            document.head.appendChild(link)
        }
    }
});